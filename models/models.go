package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// Product structure
type Product struct {
	ID            primitive.ObjectID `bson:"_id, omitempty"`
	Title         string             `bson:"title, omitempty"`
	Image         string             `bson:"image, omitempty"`
	Category      string             `bson:"category, omitempty"`
	SubCategories []string           `bson:"subCategories, omitempty"`
	SalePrice     int                `bson:"salePrice, omitempty"`
	Price         int                `bson:"price, omitempty"`
	Description   string             `bson:"description, omitempty"`
	Supplier      string             `bson:"supplier, omitempty"`
	Sku           string             `bson:"SKU, omitempty"`
	OnSale        bool               `bson:"onSale, omitempty"`
	Specs         []Specs            `bson:"specs"`
	Reviews       []Review           `bson:"reviews, omitempty"`
	Video         string             `bson:"video, omitempty"`
	Images        []string           `bson:"images, omitempty"`
	StripeID      string             `bson:"stripeID, omitempty"`
	StripePriceID string             `bson:"stripePriceID, omitempty"`
	stripeSKUID   string             `bson:"stripeSKUID, omitempty"`
}

//Review : Structure
type Review struct {
	name    string `bson:"name, omitempty"`
	date    string `bson:"date, omitempty"`
	comment string `bson:"comment, omitempty"`
	rating  int16  `bson:"rating, omitempty"`
}

//Specs : Structure
type Specs struct {
	Title string `bson:"title, omitempty"`
	Spec  string `bson:"spec, omitempty"`
}

// Category structure
type Category struct {
	ID              primitive.ObjectID `bson:"_id, omitempty"`
	Title           string             `bson:"title, omitempty"`
	Image           string             `bson:"image, omitempty"`
	Description     string             `bson:"description, omitempty"`
	Category        string             `bson:"category, omitempty"`
	SubCategories   []string           `bson:"subCategories, omitempty"`
	FullDescription string             `bson:"fullDescription, omitempty"`
	topBanner       string             `bson:"topBanner, omitempty"`
}

// Part : Structure
type Part struct {
	ID             primitive.ObjectID `bson:"_id, omitempty"`
	DateAdded      string             `bson:"dateAdded, omitempty"`
	Modified       string             `bson:"modified, omitempty"`
	StockCount     string             `bson:"stockCount, omitempty"`
	PartName       string             `bson:"partName, omitempty"`
	Thumbnail      string             `bson:"thumbnail, omitempty"`
	Stock          string             `bson:"stock, omitempty"`
	location       string             `bson:"location, omitempty"`
	SKU            string             `bson:"SKU, omitempty"`
	PartNumber     string             `bson:"partNumber, omitempty"`
	price          string             `bson:"price, omitempty"`
	Location       string             `bson:"Location, omitempty"`
	AddedBy        string             `bson:"addedBy, omitempty"`
	ShowOnWebStore bool               `bson:"showOnWebStore, omitempty"`
	Manufacturer   string             `bson:"manufacturer, omitempty"`
	Supplier       string             `bson:"supplier, omitempty"`
}

// StoreOrder Structure
type StoreOrder struct {
	ID                    primitive.ObjectID `bson:"_id, omitempty"`
	OrderID               string
	CustomerID            string
	PaymentID             string
	CheckoutID            string
	EventID               string
	InvoiceID             string
	CustomerName          string
	CustomerEmail         string
	CustomerPhone         string
	DeliveryInfo          []string
	ShippingID            string
	Items                 []string
	OrderTotalPrice       int
	PaymentAmountRecieved int
	AdditionalNotes       string
	OrderStatus           string
	DeliveryStatus        string
	Delivered             bool
	PaymentType           string
	Refunded              bool
	LastModified          string
	DateCreated           string
	RiskScore             string
	RiskLevel             string
}
