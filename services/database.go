package database

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	// "github.com/dufferzz/DFZAPI-GoLang/config"
	"github.com/dufferzz/DFZApi-GoLang/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//ConnectDB : Connect to database
func ConnectDB() *mongo.Collection {
	//Initialise env variables
	conf := config.New()

	clientOptions := options.Client().ApplyURI(conf.DbURL)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	log.Print("Connected to MongoDB!")

	collection := client.Database("diaproff").Collection("products")

	return collection
}

// ErrorResponse : Structure
type ErrorResponse struct {
	StatusCode   int    `json:"status"`
	ErrorMessage string `json:"message"`
}

// GetError : Handle error function
func GetError(err error, w http.ResponseWriter) {
	log.Fatal(err.Error())
	var response = ErrorResponse{
		ErrorMessage: err.Error(),
		StatusCode:   http.StatusInternalServerError,
	}
	message, _ := json.Marshal(response)
	w.WriteHeader(response.StatusCode)
	w.Write(message)
}
