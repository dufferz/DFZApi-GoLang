package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/dufferzz/DFZApi-GoLang/config"
	models "github.com/dufferzz/DFZApi-GoLang/models"
	database "github.com/dufferzz/DFZApi-GoLang/services"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
)

// init is invoked before main()
func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}

}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", errorHandler).Methods("GET")
	r.HandleFunc("/v2/", errorHandler).Methods("GET")
	r.HandleFunc("/v2/products/", listAllProducts).Methods("GET")
	r.HandleFunc("/v2/products/{id}", listOneProduct).Methods("GET")
	r.HandleFunc("/v2/products/sale", listSaleProducts).Methods("GET")
	r.HandleFunc("/v2/categories", listCategories).Methods("GET")
	r.HandleFunc("/v2/categories/{id}", listOneCategory).Methods("GET")
	http.ListenAndServe(":3001", r)

}

//pokeApi : Request data from live API. Testing Http Client use structure
func pokeApi() {
	conf := config.New()

	// Generate a parsable body
	reqBody := ioutil.NopCloser(strings.NewReader(`{
		"username": "dufferz",
		"password": "` + conf.TEMP + `"}`))

	//Define HTTPClient and timeout
	timeout := time.Duration(5 * time.Second) // Set timeout to 5 * Second
	client := http.Client{Timeout: timeout}

	//Define Url
	url, _ := url.Parse("https://api.dufferz.net/login")

	// Create request
	request := &http.Request{
		Method: "POST",
		URL:    url,
		Header: map[string][]string{
			"Content-Type": {"application/json; charset=UTF-8"},
		},
		Body: reqBody,
	}

	// Resp = do(request)
	resp, errr := client.Do(request)
	if errr != nil {
		log.Fatal(errr)
	}
	// Ensure closing of req
	defer resp.Body.Close()

	//parse response
	body, err := ioutil.ReadAll(resp.Body)
	// headers, err := ioutil.ReadAll(resp)

	// Strip cookies from response
	cookies := resp.Cookies()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(body))
	log.Println(cookies)
}

func errorHandler(w http.ResponseWriter, r *http.Request) {
	// Get Vars from URL
	vars := mux.Vars(r)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Category: %v\n", vars["category"])

}
func listAllProducts(w http.ResponseWriter, r *http.Request) {
	pokeApi()

	w.Header().Set("Content-Type", "application/json")

	//Connect to DB
	var products []models.Product

	collection := database.ConnectDB()

	// bson.M{},  we passed empty filter. So we want to get all data.
	cur, err := collection.Find(context.TODO(), bson.M{})

	if err != nil {
		database.GetError(err, w)
		return
	}

	// Close the cursor once finished
	/*A defer statement defers the execution of a function until the surrounding function returns.
	simply, run cur.Close() process but after cur.Next() finished.*/
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

		// create a value into which the single document can be decoded
		var product models.Product
		// & character returns the memory address of the following variable.
		err := cur.Decode(&product) // decode similar to deserialize process.
		if err != nil {
			log.Fatal(err)
		}

		// add item our array
		products = append(products, product)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(products) // encode similar to serialize process.

	// fmt.Fprint(w, "")

}
func listOneProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, vars["id"])
}

func listSaleProducts(w http.ResponseWriter, r *http.Request) {

}

func listCategories(w http.ResponseWriter, r *http.Request) {

}

func listOneCategory(w http.ResponseWriter, r *http.Request) {

}

func homeHandler(w http.ResponseWriter, r *http.Request) {

}
