# This is DFZ API, but in GoLang

Started at ~1am.. :rolleyes:

## Development Live Reload

```bash
    export PATH="$HOME/go/bin:$PATH"
    gin run main.go
```

## Production Deploypment

```bash
    [Unit]

    [Install]
    WantedBy=multi-user.target

    [Service]
    ExecStart=/usr/local/bin/<MY_GO_APP>
    WorkingDirectory=/home/user/<MY_GO_APP_HOME_DIR>
    User=<MY_GO_APP_USER>
    Restart=always
    RestartSec=5
    StandardOutput=syslog
    StandardError=syslog
    SyslogIdentifier=%n
```

Create this file as /etc/systemd/system/my_app.service, then run:

```bash
    # systemctl start my_app.service
```
